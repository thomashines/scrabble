#!/usr/bin/env python3

import math

import svgwrite

def vec_polar(angle, length = 1.0):
  return vec_scale((math.cos(angle), math.sin(angle)), length)

def vec_add(left, right):
  return list(l + r for l, r in zip(left, right))

def vec_mul(left, right):
  return list(l * r for l, r in zip(left, right))

def vec_scale(vec, factor):
  return list(v * factor for v in vec)

def vec_sub(left, right):
  return list(l - r for l, r in zip(left, right))

def vec_map(vec, function):
  return list(map(function, vec))

def vec_angle(vec):
  return math.atan2(vec[1], vec[0])

def vec_dot(left, right):
  return sum(l + r for l, r in zip(left, right))

def vec_length(vec):
  return math.sqrt(vec_dot(vec, vec))

def angle_normalise(angle):
  while angle < -math.pi:
    angle += 2.0 * math.pi
  while angle > math.pi:
    angle -= 2.0 * math.pi
  return angle

def vecs_angle(left, right):
  return angle_normalise(vec_angle(right) - vec_angle(left))

def draw_line_rounded(drawing, points, radius, width):

  forwards = []
  backwards = []

  radius_end = 0.5 * width
  radius_inner = radius
  radius_outer = radius + width
  radius_centre = 0.5 * (radius_inner + radius_outer)

  for pi, p1 in enumerate(points):

    p0 = None if (pi == 0) else points[pi - 1]
    p2 = None if (pi == len(points) - 1) else points[pi + 1]

    if p0 is not None and p2 is not None:
      vecs = (vec_sub(p1, p0), vec_sub(p2, p1))
      angles = tuple(math.atan2(vec[1], vec[0]) for vec in vecs)
      angle_diff = angle_normalise(angle_normalise(angles[1] + math.pi) - angles[0])
      angle_centre = angle_normalise(angles[1] - 0.5 * angle_diff)
      centre = vec_add(p1, vec_polar(angle_centre, abs(radius_centre / math.sin(0.5 * angle_diff))))
      angle0 = angle_normalise(angles[0] + math.copysign(0.5 * math.pi, angle_diff))
      angle1 = angle_normalise(angles[1] + math.copysign(0.5 * math.pi, angle_diff))
      inner_vertices = (
        (pi, radius_inner, 0, vec_add(centre, vec_polar(angle0, radius_inner))),
        (pi, radius_inner, 0, vec_add(centre, vec_polar(angle1, radius_inner)))
      )
      outer_vertices = (
        (pi, radius_outer, 1, vec_add(centre, vec_polar(angle0, radius_outer))),
        (pi, radius_outer, 1, vec_add(centre, vec_polar(angle1, radius_outer)))
      )
      if angle_diff > 0:
        forwards.extend(inner_vertices)
        backwards.extend(outer_vertices)
      elif angle_diff < 0:
        forwards.extend(outer_vertices)
        backwards.extend(inner_vertices)

    elif p0 is not None:
      vec = vec_sub(p1, p0)
      angle = math.atan2(vec[1], vec[0])
      angle_forwards = angle - 0.5 * math.pi
      forwards.append((pi, radius_end, 1, vec_add(p1, vec_polar(angle_forwards, radius_end))))
      angle_backwards = angle + 0.5 * math.pi
      backwards.append((pi, radius_end, 1, vec_add(p1, vec_polar(angle_backwards, radius_end))))

    elif p2 is not None:
      vec = vec_sub(p2, p1)
      angle = math.atan2(vec[1], vec[0])
      angle_forwards = angle - 0.5 * math.pi
      forwards.append((pi, radius_end, 1, vec_add(p1, vec_polar(angle_forwards, radius_end))))
      angle_backwards = angle + 0.5 * math.pi
      backwards.append((pi, radius_end, 1, vec_add(p1, vec_polar(angle_backwards, radius_end))))

  backwards.reverse()
  line = forwards + backwards

  path = drawing.path(stroke="red", fill="grey")
  path.push("M", *line[0][3])

  for lpi in range(len(line)):
    pi, radius, sweep_flag, _ = line[lpi]
    next_lpi = (lpi + 1) % len(line)
    next_pi, _, _, next_vertex = line[next_lpi]
    if pi == next_pi:
      path.push("A", radius, radius, 0, 0, sweep_flag, *next_vertex)
    else:
      path.push("L", *next_vertex)

  path.push("Z")

  drawing.add(path)

def outline(vertices):
  # vertices is of a polygon in clockwise order
  # the polygon can be non-convex and have edges that intersect each other
  # return one list of the outside edges of the polygon and another list of all
  # the holes inside the polygon

  # start by building a graph of all the vertices and intersections in the
  # polygon and the edges that connect them

  # build the outside by:
  #   * start at the vertex with the lowest x value of the vertices with the
  #     lowest y value
  #   * step to the next connected vertex clockwise of the vector (0, -1)
  #   * keep stepping clockwise along the outside like a convex hull

  # build the inside by:

  outside = vertices
  insides = []

  def find_intersection(l00, l01, l10, l11):
    return (0.0, 0.0)

  return outside, insides

def draw_line_sharp(drawing, width, points):
  forwards = []
  backwards = []

  for pi, (p1, a1) in enumerate(points):

    p0, a0 = (None, None) if (pi == 0) else points[pi - 1]
    p2, a2 = (None, None) if (pi == len(points) - 1) else points[pi + 1]

    if p0 is None:
      p12 = vec_sub(p2, p1)
      p12_angle = vec_angle(p12)
      p12_angle_diff = angle_normalise(a1 - p12_angle)
      p12_offset = width * 0.5 / math.sin(p12_angle_diff)
      p12_offset_vector = vec_polar(a1, p12_offset)
      forwards.append(vec_sub(p1, p12_offset_vector))
      backwards.append(vec_add(p1, p12_offset_vector))

    elif p2 is None:
      p01 = vec_sub(p1, p0)
      p01_angle = vec_angle(p01)
      p01_angle_diff = angle_normalise(a1 - p01_angle)
      p01_offset = width * 0.5 / math.sin(p01_angle_diff)
      p01_offset_vector = vec_polar(a1, p01_offset)
      forwards.append(vec_sub(p1, p01_offset_vector))
      backwards.append(vec_add(p1, p01_offset_vector))

    else:
      p01 = vec_sub(p1, p0)
      p01_angle = vec_angle(p01)

      p12 = vec_sub(p2, p1)
      p12_angle = vec_angle(p12)

      p01_12_angle = vecs_angle(p01, p12)
      p01_12_angle_supplement = angle_normalise(math.pi - p01_12_angle)

      # Inner vertex
      inner_offset = 0.5 * width / math.sin(abs(p01_12_angle_supplement) / 2.0)
      inner_angle = p12_angle + 0.5 * p01_12_angle_supplement
      inner_vertex = vec_add(p1, vec_polar(inner_angle, inner_offset))
      if p01_12_angle >= 0.0:
        backwards.append(inner_vertex)
      else:
        forwards.append(inner_vertex)

      # Outer vertices

      p01_angle_diff = angle_normalise(a1 - p01_angle)
      p01_offset = width * 0.5 / math.sin(p01_angle_diff)
      p01_offset_vector = vec_polar(a1, p01_offset)
      if p01_12_angle >= 0.0:
        forwards.append(vec_sub(p1, p01_offset_vector))
      else:
        backwards.append(vec_add(p1, p01_offset_vector))

      p12_angle_diff = angle_normalise(a1 - p12_angle)
      p12_offset = width * 0.5 / math.sin(p12_angle_diff)
      p12_offset_vector = vec_polar(a1, p12_offset)
      if p01_12_angle >= 0.0:
        forwards.append(vec_sub(p1, p12_offset_vector))
      else:
        backwards.append(vec_add(p1, p12_offset_vector))

  backwards.reverse()
  outside, insides = outline(forwards + backwards)

  path = drawing.path(stroke="red", fill="grey")
  path.push("M", *outside[0])
  for vertex in outside[1:]:
    path.push("L", *vertex)
  path.push("Z")

  drawing.add(path)

def draw_box(drawing, centre, size, gap, radius, bridge):
  corners = ((-1, -1), (1, -1), (1, 1), (-1, 1))
  corners = list(vec_add(centre, vec_mul(size, vec_scale(corner, 0.5))) for corner in corners)

  for c1 in corners:
    c1_to_centre = vec_sub(centre, c1)
    c1_to_centre_sign = vec_map(c1_to_centre, lambda x: math.copysign(1.0, x))
    c1_to_bridge_start = vec_sub(c1_to_centre, vec_scale(c1_to_centre_sign, 0.5 * (bridge + gap)))
    c0 = vec_add(c1, (0.0, c1_to_bridge_start[1]))
    c2 = vec_add(c1, (c1_to_bridge_start[0], 0.0))
    draw_line_rounded(drawing, (c0, c1, c2), radius, gap)

def main():
  print("scrabble.py")

  scale = 16
  gap_width = 2.5 * scale
  canvas = vec_scale((32, 32), scale)
  centre = vec_scale(canvas, 0.5)

  drawing = svgwrite.Drawing("w.svg", size=canvas)

  outline_size = vec_scale((1, 1), 24 * scale + gap_width)
  outline_radius = 1 * scale
  outline_bridge = gap_width
  draw_box(drawing, centre, outline_size, gap_width, outline_radius, outline_bridge)

  w = (
    (-1.0, -1.0),
    (-0.5,  1.0),
    ( 0.0, -1.0),
    ( 0.5,  1.0),
    ( 1.0, -1.0),
  )
  w = tuple(vec_add(centre, vec_scale(vec, 8 * scale)) for vec in w)
  w = tuple((vec, 0.0) for vec in w)
  draw_line_sharp(drawing, gap_width, w)

  four_points = (
    ( 1.0,  0.0),
    (-1.0,  0.0),
    ( 0.4, -1.8),
    ( 0.4,  0.8),
  )
  four_angles = (
    math.pi / 2.0,
    math.pi / 2.0,
              0.0,
              0.0,
  )
  four_centre = vec_add(centre, vec_scale(outline_size, 0.33))
  four_points = tuple(vec_add(four_centre, vec_scale(vec, 2 * scale)) for vec in four_points)
  four = tuple(x for x in zip(four_points, four_angles))
  draw_line_sharp(drawing, 1.0 * scale, four)

  drawing.save(pretty=True)

if __name__ == "__main__":
  main()
